# Stegnuti Weather

Offers the user to visualize weather predictions (temperature, humidity 
and pressure) via graph or table for a custom set of cities within a
user-defined time interval anywhere between now and 7 days in the future.

Pulls data from Open Weather Map API (https://openweathermap.org/api).