﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net;
using Newtonsoft.Json;
using System.IO;
using DevExpress.Xpf.Charts;
using System.ComponentModel;

namespace StegnutiWeather
{
    public enum Parameter { TEMPMAX, TEMPCUR, TEMPMIN, HUMIDITY, PRESSURE, VISIBILITY }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<WeatherReading> readings;
        private static string apiUrl = "https://api.openweathermap.org/data/2.5/";
        private static string apiKey = "a63e03341304035cbeccdfb47c0617c6";

        private static string locationsPath = "../../cities.json";
        public Dictionary<string, CityInfo> allLocations;
        public Dictionary<string, CityInfo> watchedLocations;
        public Dictionary<string, Weather> locationWeather;

        private DateTime from, to;
        public MainWindow()
        {
            InitializeComponent();

            LoadCities();
            watchedLocations = new Dictionary<string, CityInfo>();
            locationWeather = new Dictionary<string, Weather>();

            // Let the user pick between now and 7 days in the future
            from = DateTime.Now;
            to = DateTime.Now.AddDays(7);

            date_From.DisplayDate = from;
            date_To.DisplayDate = to;

            date_From.SelectedDate = from;
            date_To.SelectedDate = to;

            date_From.DisplayDateStart = from;
            date_From.DisplayDateEnd = to;

            date_To.DisplayDateStart = from;
            date_To.DisplayDateEnd = to;

            date_From.IsTodayHighlighted = true;
            date_To.IsTodayHighlighted = true;
        }

        /// <summary>
        /// Load selectable locations from json
        /// </summary>
        private void LoadCities()
        {
            string jsonStr = System.IO.File.ReadAllText(locationsPath);
            List<CityInfo> locations = JsonConvert.DeserializeObject<CityList>(jsonStr).cities;
            allLocations = new Dictionary<string, CityInfo>();
            foreach (CityInfo city in locations)
            {
                allLocations[city.name] = city;
            }
        }

        public async Task PullLocationWeatherAsync(CityInfo cityInfo, Action callback)
        {
            string url = apiUrl + String.Format("onecall?lat={0}&lon={1}&appid={2}", cityInfo.coord.lon, cityInfo.coord.lat, apiKey);
            // Create a request for the URL. 		
            WebRequest request = WebRequest.Create(url);
            // Get the response.
            HttpWebResponse response = (HttpWebResponse) await request.GetResponseAsync();
            // Get the stream containing content returned by the server.
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
            // Cleanup the streams and the response.
            reader.Close();
            dataStream.Close();
            File.WriteAllText("response.txt", response.StatusDescription + ", " + responseFromServer);
            response.Close();

            API.Response responseObj = JsonConvert.DeserializeObject<API.Response>(responseFromServer);
            locationWeather[cityInfo.name] = new Weather(responseObj);

            callback();
        }

        private void PullLocationWeather(CityInfo cityInfo)
        {
            string url = apiUrl + String.Format("onecall?lat={0}&lon={1}&appid={2}", cityInfo.coord.lon, cityInfo.coord.lat, apiKey);
            // Create a request for the URL. 		
            WebRequest request = WebRequest.Create(url);
            // Get the response.
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            // Get the stream containing content returned by the server.
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
            // Cleanup the streams and the response.
            reader.Close();
            dataStream.Close();
            File.WriteAllText("response.txt", response.StatusDescription + ", " + responseFromServer);
            response.Close();

            API.Response responseObj = JsonConvert.DeserializeObject<API.Response>(responseFromServer);
            locationWeather[cityInfo.name] = new Weather(responseObj);
        }

        public void ShowWeather(CityInfo city)
        {
            if (!locationWeather.ContainsKey(city.name))
            { 
                PullLocationWeather(city);
            }

            // Clear and populate datagrid
            dataGrid_main.ItemsSource = locationWeather[city.name].GetWeatherReadings(from, to);

            // Populate chart
            btn_Temperature_Click(null, null);

            // Populate current weather values
            WeatherReading cw = locationWeather[city.name].current;
            txt_CurTemp.Text = cw.TempCurrent.ToString();
            txt_CurPress.Text = cw.Pressure.ToString();
            txt_CurHumid.Text = cw.Humidity.ToString();
            txt_CurVis.Text = cw.Visibility.ToString();

            lbl_ActiveCity.Content = "Weather for " + city.name + " at " + cw.Time.ToString();
        }

        private void PopulateChart(Parameter parameter, DateTime from, DateTime to)
        {
            // Clear diagram
            chart_diagram.Series.Clear();

            foreach (CityInfo watched in watchedLocations.Values)
            {
                // Pull weather info if not present
                if (!locationWeather.ContainsKey(watched.name))
                {
                    PullLocationWeather(watched);
                }

                // Draw the line for this city
                LineSeries2D series = new LineSeries2D();
                series.LineStyle = new LineStyle(4);
                series.DisplayName = watched.name;

                foreach (WeatherReading reading in locationWeather[watched.name].GetWeatherReadings(from, to))
                {
                    series.Points.Add(new SeriesPoint(reading.Time, reading.GetValue(parameter)));
                }

                chart_diagram.Series.Add(series);
            }

            chart_diagram.DependentAxesYRange = true;
        }



        private RadioButton AddToSidebar(CityInfo cityInfo)
        {
            foreach (RadioButton child in stack_LeftBar.Children)
            {
                if (child.Content.ToString() == cityInfo.name)
                {
                    return child;
                }
            }
            RadioButton btn = new RadioButton();
            btn.Content = cityInfo.name;
            btn.Height = 40;
            btn.Margin = new Thickness(0, 0, 0, 16);
            btn.FontSize = 20;
            btn.Click += btn_CityRadio_Click;
            btn.MouseRightButtonUp += OnTryDeleteLocation;
            stack_LeftBar.Children.Add(btn);

            return btn;
        }

        private void OnTryDeleteLocation(object sender, EventArgs e)
        {
            string cityName = ((RadioButton)sender).Content.ToString();
            MessageBoxResult result = MessageBox.Show("Delete " + cityName + "?", "Delete city", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                watchedLocations.Remove(cityName);
                stack_LeftBar.Children.Remove((RadioButton)sender);
                Refresh();
            }
        }

        private void Refresh() 
        {
            if (watchedLocations.Count == 0)
            {
                // Clear and populate datagrid
                dataGrid_main.ItemsSource = null;

                // Populate chart
                btn_Temperature_Click(null, null);

                // Populate current weather values
                txt_CurTemp.Text = "";
                txt_CurPress.Text = "";
                txt_CurHumid.Text = "";
                txt_CurVis.Text = "";

                lbl_ActiveCity.Content = "City list is empty.";
            }
        }

        public RadioButton AddLocation(string cityName)
        {
            // Add to watched locations
            CityInfo cityInfo = allLocations[cityName];
            if (!watchedLocations.ContainsKey(cityInfo.name))
            { 
                watchedLocations[cityInfo.name] = cityInfo;
            }

            // Generate new sidebar button and retrieve it
            RadioButton newBtn = AddToSidebar(cityInfo);
            newBtn.IsChecked = true;
            return newBtn;
        }

        private void OnCitySelected(string name)
        {
            CityInfo city = watchedLocations[name];
            ShowWeather(city);
            
        }

        private void InitializeMainView(string cityName)
        {
            AddLocation(cityName);
            PullLocationWeatherAsync(watchedLocations[cityName], () => {
                can_loading.Visibility = Visibility.Hidden;
                grid_main.Visibility = Visibility.Visible;

                ShowWeather(watchedLocations[cityName]);
                
            });
        }


        /*/////////////////////////
         CALLBACKS
        //////////////////////////
        ///
        */

        private void btn_CityRadio_Click(object sender, RoutedEventArgs e)
        {
            OnCitySelected(((RadioButton)sender).Content.ToString());
        }

        private void btn_AddLocation_Click(object sender, RoutedEventArgs e)
        {
            AddLocationDialog dialog = new AddLocationDialog();
            dialog.Owner = this;
            dialog.ShowDialog();
        }

        private void date_To_CalendarOpened(object sender, RoutedEventArgs e)
        {
            // Display only dates from 'from' to 7 days from now
            date_To.DisplayDateStart = from;
        }

        private void date_From_CalendarOpened(object sender, RoutedEventArgs e)
        {
            // Display only dates from now to 'to'
            date_From.DisplayDateEnd = to;
        }

        private void date_From_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (date_From.SelectedDate != null) from = (DateTime) date_From.SelectedDate;
            Refresh();
        }

        private void date_To_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (date_To.SelectedDate != null) to = (DateTime)date_To.SelectedDate;
            Refresh();
        }

        private void btn_Pressure_Click(object sender, RoutedEventArgs e)
        {
            PopulateChart(Parameter.PRESSURE, from, to);

            // Color only this button and gray the rest
            btn_Pressure.Opacity = 1;
            btn_Temperature.Opacity = 0.5;
            btn_Humidity.Opacity = 0.5;
            btn_Visibility.Opacity = 0.5;
        }

        private void btn_Humidity_Click(object sender, RoutedEventArgs e)
        {
            PopulateChart(Parameter.HUMIDITY, from, to);

            // Color only this button and gray the rest
            btn_Pressure.Opacity = 0.5;
            btn_Temperature.Opacity = 0.5;
            btn_Humidity.Opacity = 1;
            btn_Visibility.Opacity = 0.5;
        }

        private void btn_Temperature_Click(object sender, RoutedEventArgs e)
        {
            PopulateChart(Parameter.TEMPCUR, from, to);

            // Color only this button and gray the rest
            btn_Pressure.Opacity = 0.5;
            btn_Temperature.Opacity = 1;
            btn_Humidity.Opacity = 0.5;
            btn_Visibility.Opacity = 0.5;
        }

        private void btn_Visibility_Click(object sender, RoutedEventArgs e)
        {
            PopulateChart(Parameter.VISIBILITY, from, to);

            // Color only this button and gray the rest
            btn_Pressure.Opacity = 0.5;
            btn_Temperature.Opacity = 0.5;
            btn_Humidity.Opacity = 0.5;
            btn_Visibility.Opacity = 1;
        }


        private void txtSearch_Filter(object sender, EventArgs e)
        {
            lst_Suggestions.Visibility = Visibility.Visible;
            lst_Suggestions.Items.Clear();
            string search = txt_CityName.Text.ToLower();
            foreach (string name in allLocations.Keys)
            {
                if (name.ToLower().StartsWith(search))
                {
                    lst_Suggestions.Items.Add(name);
                }
            }
        }

        private void lst_Suggestions_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            can_welcome.Visibility = Visibility.Hidden;
            can_loading.Visibility = Visibility.Visible;
            InitializeMainView(lst_Suggestions.SelectedItem.ToString());
        }

        
    }
}
