﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace StegnutiWeather
{
    /// <summary>
    /// Interaction logic for AddLocationDialog.xaml
    /// </summary>
    public partial class AddLocationDialog : Window
    {
        public AddLocationDialog()
        {
            InitializeComponent();
        }

        private void txtSearch_Filter(object sender, EventArgs e)
        {
            lst_Suggestions.Visibility = Visibility.Visible;
            lst_Suggestions.Items.Clear();
            string search = txt_CityName.Text.ToLower();
            foreach (string name in ((MainWindow) Owner).allLocations.Keys)
            {
                if (name.ToLower().StartsWith(search))
                {
                    lst_Suggestions.Items.Add(name);
                }
            }
        }

        private void lst_Suggestions_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            can_welcome.Visibility = Visibility.Hidden;
            can_loading.Visibility = Visibility.Visible;
            string cityName = lst_Suggestions.SelectedItem.ToString();
            MainWindow main = (MainWindow)Owner;

            main.AddLocation(cityName);
            main.PullLocationWeatherAsync(main.allLocations[cityName], () => {
                Close();
                main.ShowWeather(main.watchedLocations[cityName]);
            });
        }
    }
}
