﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StegnutiWeather
{
    namespace API
    { 
        public class Response
        {
            public CurrentWeather current;
            public List<HourWeather> hourly;
            public List<DayWeather> daily;
        }

        public class CurrentWeather
        {
            public long dt; // Timestamp
            public double temp, pressure, humidity, visibility;
        }

        public class HourWeather
        {
            public long dt; // Timestamp
            public double temp, pressure, humidity, visibility;
        }

        public class DayWeather
        {
            public long dt; // Timestamp
            public Temp temp;
            public double pressure, humidity, visibility;
        }

        public class Temp
        {
            public double min, max, day;
        }
    }
}
